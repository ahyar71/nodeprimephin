'use strict';
exports.plugin = {  
    pkg: require('../../../package.json'),
    name : 'prime_routes',
    register: async (server, options) => {
        const Controllers = {
            prime: {
                prime: require('../../controllers/web/prime')
            }
        };
        server.route([
            {
                method: 'GET',
                path: '/prime',
                config: Controllers.prime.prime.showPrimeInit
            },
            {
                method: 'POST',
                path: '/prime',
                config: Controllers.prime.prime.PostPrimeInit
            },
            {
                method: 'GET',
                path: '/prime/{number}',
                config: Controllers.prime.prime.ShowPrimeResult
            },
        ]);
    }
};

