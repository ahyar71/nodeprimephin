'use strict';

exports.plugin = {  
    pkg: require('../../../../package.json'),
    name : 'prime_routes_v1',
    register: async (server, options) => {
        const Controllers = {
            prime: {
                prime: require('../../../controllers/api/prime')
            }
        };
        const basePath = '/api/v1/';
        server.route([
            {
                method: 'POST',
                path: basePath + 'prime',
                config: Controllers.prime.prime.primeCheck
            }
        ]);
    }
};
