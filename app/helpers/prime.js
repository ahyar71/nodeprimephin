exports.primeResult = async function (number) {
    return new Promise(async function (resolve, reject) {
        try {
            console.log(number)
            if (number < 2) {
                res = 'Number must be greater than 2.';
                resolve(res);
            }
            const sq_root = parseInt(Math.sqrt(number));
            for (let i=2; i<=sq_root; i++){
                if (number % i == 0) {
                    res = number+` is not Prime`;
                    return resolve(res);
                }
            }
            res = number+` is Prime`;
            return resolve(res);
        } catch (error) {
            return reject(error);
        }
    });
};