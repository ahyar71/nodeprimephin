'use strict';

const Joi = require('joi');
const primeHelper = require('../../helpers/prime');

exports.showPrimeInit = {
  description: 'Returns the prime initial page',
  auth: {
      mode: 'try',
      strategy: 'standard'
  },
  handler: async (request, h) => {
    try {
      if (request.auth.isAuthenticated) {
        var userDetails = request.auth.credentials;
        if (request.query.number) {
          return h.redirect('/prime/'+request.query.number, {user: userDetails});
        }
        return h.view('prime/prime', {user: userDetails});
      }
      return h.redirect('/login')
    } catch (error) {
      console.log(error)
      return h.redirect('/');  
    }
  }
};

exports.PostPrimeInit = {
  description: 'Post to prime initial page',
  auth: {
    mode: 'try',
    strategy: 'standard'
  },
  validate: {
    payload: {
      number: Joi.number().integer().required()
    },
    failAction: (request, h, error) => {
        request.yar.flash('error', error.details[0].message.replace(/['"]+/g, ''));
        return h.redirect('/prime').takeover();
    }
},
  handler: async (request, h) => {
    try {
      if (request.auth.isAuthenticated) {
        console.log(request.payload.number)
        var userDetails = request.auth.credentials;
        return h.redirect('/prime/'+request.payload.number, {user: userDetails});
      }
      return h.redirect('/login')
    } catch (error) {
      console.log(error)
        return h.redirect('/');  
    }
  }
};

exports.ShowPrimeResult = {
  description: 'Show the prime result page',
  auth: {
      mode: 'try',
      strategy: 'standard'
  },
  handler: async (request, h) => {
    try {
      if (request.auth.isAuthenticated) {
        let res = await primeHelper.primeResult(request.params.number)
        request.yar.flash('success', res);
        return h.redirect('/');
      }
    } catch (error) {
      console.log(error)
        return h.redirect('/');  
    }
  }
};

var counting = (number) => {
    if (number < 2) {
      res = 'Number must be greater than 2.';
      return res;
    }
    const sq_root = parseInt(Math.sqrt(number));
    for (let i=2; i<=sq_root; i++){
      if (number % i == 0) {
        res = number+` is not Prime`;
        return res;
      }
    }
    res = number+` is Prime`;
    return res;
  };