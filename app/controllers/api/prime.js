'use strict';
var Boom = require('boom');
var JWT   = require('jsonwebtoken');
const Joi = require('joi');
const Config = require('../../../config/config');
const Mongoose = require('mongoose');
const User = Mongoose.model('User');
const signupHelper = require('../../helpers/signup');
const primeHelper = require('../../helpers/prime');

/* ================================== Controllers for V1 ============================== */

// validate user login.
exports.primeCheck = {
    description: 'Check if integer is a prime number or not',
    auth : false,
    validate: {
        payload: {
            number: Joi.number().integer().required(),
        },
         failAction: (request, h, error) => {
            return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
        }
    },
    handler: async (request, h) => {
        try {
            let number = request.payload.number;
            let res = await primeHelper.primeResult(number)
            return h.response({ message: res }).code(201);
        } catch (error) {
            console.log(error)
            return error.message;
        }  
    },
    tags: ['api'] //swagger documentation
};


/* ================================== Controllers for V2 ============================== */
